#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
    using std::ofstream;
#include <errno.h>
#include <iostream>
#include <chrono>
#include <string>
#include <iostream>
#include "boost/filesystem.hpp"
#include <thread>
#include "../lib/INIReader.h"

using namespace boost::filesystem;
using namespace std;
using namespace std::chrono;





// INIReader.h --- https://github.com/jtilly/inih

//
// interact with the .ini file
//
int getMd5ChallengeSearchStringSize()
{
    // get the value from the .ini

    INIReader reader("config/cluster_config.ini");

    // make sure we can read something
    if (reader.ParseError() < 0)
    {
        std::cout << "LEADER:: Error: Can't load 'cluster_config.ini'.\n";

        return -1;
    }

    int searchStringSize = reader.GetInteger("Md5Challenge", "SearchStringSize", -1);

    std::cout << "LEADER:: Config loaded: [Md5Challenge] SearchStringSize="
              << searchStringSize << "\n";

    return searchStringSize;
}

//
// interact with the .ini file
//
string getMd5ChallengeRootPath()
{
    // get the value from the .ini

    INIReader reader("config/cluster_config.ini");

    // make sure we can read something
    if (reader.ParseError() < 0)
    {
        std::cout << "LEADER:: Error: Can't load 'cluster_config.ini'.\n";

        return "INI PARSE ERROR";
    }

    string rootPath = reader.Get("Md5Challenge", "RootPath", "INI PARSE ERROR");

    std::cout << "LEADER:: Config loaded: [Md5Challenge] RootPath="
              << rootPath << "\n";

    return rootPath;
}





















//
// summary: read the text file into memory, and then walk it, character by character
//
// TODO/FLAWS:
//     1) lines with only \n and only \r are not currently handled
//        - the logic only looks for \r\n pairs during preprocessing
//     2) this is only a performance flaw, since I could be deleting more characters
//        - when multiple \r\n appear in a row, i will only remove preceeding characters
//          starting at the last pair, which means, I'm losing (searchWindowSize * \r\n pairs)
//          number of characters that could've been safely deleted, but weren't.
//
bool preProcessTextFileInMemory(string targetFilePath, int searchStringSize)
{
    // LOG: INFO
    cout << "LEADER:: start preProcessTextFileInMemory()" << endl;

    // create target file path in proper format --- fix this so it uses targetFilePath.length()
    char targetFilePathChars[300];
    strcpy(targetFilePathChars, targetFilePath.c_str());

    // LOG: DEBUG
    cout << "target file path has been converted from string to char[]" << endl;

    // create temp file path in proper format (char[])
    //std::string const preProcessTempFilePath = "tmp/leader_preprocess.txt";
    //char preProcessTempFilePathChars[100];
    //strcpy(preProcessTempFilePathChars, preprocessTempFilePath.c_str());

    // read whole file into memory (string)
    std::ifstream textFileIn(targetFilePathChars);
    std::string textFileContents((std::istreambuf_iterator<char>(textFileIn)),
                                  std::istreambuf_iterator<char>());

    // LOG: DEBUG
    cout << "whole file has been read to memory cache" << endl;

    // convert text file in memory from string to char[]
    int textFileLengthInChars = textFileContents.length() + 1;

    // LOG: DEBUG
    cout << "textFileLengthInChars == " << textFileLengthInChars << endl;

    //char * inMemoryCache[textFileLengthInChars]; //segfaults on large file gn06v10.txt
    char* inMemoryCache = new char[textFileLengthInChars]();

    // LOG: DEBUG
    cout << "inMemoryCache sizeof == " << sizeof(inMemoryCache) << endl;

    strcpy(inMemoryCache, textFileContents.c_str());

    // LOG: DEBUG
    //cout << inMemoryCache << endl;

    // LOG: DEBUG
    cout << "text file converted from string to char[]" << endl;

    // DEBUG
    //inMemoryCache[] = {"something1something2something3something4something5\r\n"};
    //textFileLengthInChars = strlen(inMemoryCache);

    // set number of characters to remove from either side of the newline (one less than total searchWindowSize)
    //int numberOfCharsToRemoveAroundNewlines = searchStringSize - 1;

    // LOG: DEBUG
    //cout << "numberOfCharsToRemoveAroundNewlines= " << numberOfCharsToRemoveAroundNewlines << endl;

    // we'll segfault if we get to the end of the file and then use the [index + 3]
    //     to search for \r\n, because it raises an out of bounds exception
    //     - also, imagine there was a \r\n pair about 5 chars before the end of the file
    //       this would cause the below logic to try and delete the following characters
    //       * searchStringSize.... Which would throw.
    int charsSafeToBePreProcessed = textFileLengthInChars - searchStringSize;

    // LOG: DEBUG
    cout << "charsSafeToBePreProcessed: " << charsSafeToBePreProcessed << endl;

    // LOG: INFO
    cout << "LEADER:: PreProcessing: Step 1 -- Replace newlines (and lines too short to match) with special token character \\a" << endl;

    // walk each character of the in memory cache
    for(int textFileIndex = 0; textFileIndex < charsSafeToBePreProcessed; textFileIndex++)
    {
        // LOG: DEBUG
        //cout << "LEADER:: DEBUG: textFileIndex is " << textFileIndex << endl;

        // might want to use list or vector object instead of char array -- di0
        //    -- that way i wouldn't have to deal with removing nulls

        // if current character is "\r" and next character is "\n", we've reached a newline
        if (inMemoryCache[textFileIndex] == '\r'
            && inMemoryCache[textFileIndex + 1] == '\n')
        {
            // LOG: DEBUG
            //cout << "found a newline starting at index " << textFileIndex << endl;

            // set this pair of \r and \n characters to \0
            inMemoryCache[textFileIndex] = '\a';
            inMemoryCache[textFileIndex + 1] = '\a';

            // skip ahead one character in the index, since we already know these two are a pair
            textFileIndex++;

            // support lines that are smaller than searchWindowSize
            //   - if there isn't as much space as the searchWindowSize
            //     on that line, then don't preprocess it other than removing the
            //     newline. If we removed anymore, we'd miss matches

            //int lineStartChar = textFileIndex; // on what char in the file does this newline start?

            // check to see if the line is too small to be a match
            for (int i = 0; i <= searchStringSize; i++)
            {
                // scan forward to see if the next \r\n pair comes before we hit the searchWindowSize
                if (inMemoryCache[textFileIndex + i] == '\r'
                    && inMemoryCache[textFileIndex + i + 1] == '\n')
                {
                     // LOG: DEBUG
                     //cout << "found a line too small to match -- deleting it" << endl;

                     // backtrack and fill that line in with deletion / pruning tokens
                     for (int j = 0; j <= i; j++)
                     {
                          // LOG: DEBUG
                          //cout << inMemoryCache[textFileIndex + j] << endl;

                          inMemoryCache[textFileIndex + j] = '\a';
                     }

                     break;
                }
            }
        }
    }

    // LOG: DEBUG
    cout << "LEADER:: PreProcessing: Step 2 -- copy inMemoryCache to inMemoryCacheTrimmed" << endl;

    // LOG: DEBUG
    //cout << inMemoryCache << endl;

    //
    // copy inMemoryCache to inMemoryCacheTrimmed
    //

    // create new array that is the same size
    int dataSize = strlen(inMemoryCache) + 1; // + 1 for \0 termination char

    // LOG: DEBUG
    cout << "sizeof inMemoryCache: " << dataSize << endl;

    //char inMemoryCacheTrimmed[dataSize]; // segfault on gn06v10.txt
    char* inMemoryCacheTrimmed = new char[dataSize]();

    // LOG: DEBUG
    cout << "sizeof inMemoryCacheTrimmed: " << sizeof(inMemoryCacheTrimmed) << endl;

    int trimmedIndex = 0;

    // LOG: DEBUG
    cout << "replacing \a characters with nothing by copying array" << endl;


    // for each character in inMemoryCache
    for (int i = 0; i <= dataSize; i++)
    {
        // if it's not the junk character we inserted, then write it to the new array
        if (inMemoryCache[i] != '\a')
        {
             inMemoryCacheTrimmed[trimmedIndex] = inMemoryCache[i];

             trimmedIndex++;

             // LOG: DEBUG
             //cout << inMemoryCacheTrimmed << endl;
        }
    }

    // LOG: DEBUG
    cout << "character replacement complete" << endl;

    // DEBUG: sleep so i can see what's happening
    //std::this_thread::sleep_for (std::chrono::seconds(1));

    // LOG: DEBUG
    //cout << inMemoryCacheTrimmed << endl;

    //
    // the remainder of this function outputs inMemoryCache to preprocessing temp text file
    //

    std::ofstream preProcessTempFile;

    preProcessTempFile.open("tmp/leader_preprocess.txt");

    if (!preProcessTempFile)
    {
        cout << "LEADER:: Error: Could not open preprocessing text file" << endl;

        preProcessTempFile.close();

        return false;
    }

    // perform the text file write (overwrite)
    preProcessTempFile << inMemoryCacheTrimmed;

    if (!preProcessTempFile)
    {
        cout << "LEADER:: Error: Could not write to preprocessing text file" << endl;

        preProcessTempFile.close();

        return false;
    }

    preProcessTempFile.close();

    // fix memory leaks (no GC in c++)
    delete inMemoryCache;
    delete inMemoryCacheTrimmed;

    puts("LEADER:: Completed preprocessing for this Text File.");

    puts("LEADER:: End method preProcessTextFileInMemory().");

    return true;
}



























int send_text_file(int socket, string fromFilePath)
{
    FILE *textFile;
    int size, read_size, stat, packet_index;
    char send_buffer[10240], read_buffer[256];
    packet_index = 1;

    textFile = fopen(fromFilePath.c_str(), "r");

    if(textFile == NULL)
    {
        puts("LEADER:: Error Opening Text File.");
    }

    // Get Text File Size
    puts("LEADER:: Getting Text File Size...");
    fseek(textFile, 0, SEEK_END);
    size = ftell(textFile);
    fseek(textFile, 0, SEEK_SET);
    printf("LEADER:: Total Text File size: %i.\n",size);

    // Send Text File Size to Worker Node
    puts("LEADER:: Sending Text File Size...");
    write(socket, (void *)&size, sizeof(int));

    // Send Text File as Byte Array
    puts("LEADER:: Sending Text File as Byte Array....");

    do
    { //Read while we get errors that are due to signals.
        stat = read(socket, &read_buffer, 255);
        printf("LEADER:: Bytes read: %i.\n",stat);
    }
    while (stat < 0);

    puts("LEADER:: Received data in socket.");

    // DEBUG
    //printf("Socket data: %c\n", read_buffer);

    while(!feof(textFile)) {
        //while(packet_index = 1){
        //Read from the file into our send buffer
        read_size = fread(send_buffer, 1, sizeof(send_buffer)-1, textFile);

        //Send data through our socket
        do
        {
            stat = write(socket, send_buffer, read_size);
        }
        while (stat < 0);

        // DEBUG
        //printf("Packet Number: %i\n",packet_index);
        //printf("Packet Size Sent: %i\n",read_size);
        //printf(" \n");
        //printf(" \n");

        packet_index++;

        // Zero out our send buffer
        bzero(send_buffer, sizeof(send_buffer));
    }

    // cleanup
    fclose(textFile);

    puts("LEADER:: COMPLETED file xfer to worker.");
}




// recursive method (calls itself on finding a subfolder)
int iterateRootFolder(path rootFolder, int socket_desc)
{
    int new_socket;
    int c;
    struct sockaddr_in client;

    directory_iterator end_itr;

    int searchStringSize = getMd5ChallengeSearchStringSize();

    // this waitCounter gives the workers a chance to get started
    //    before preprocessing any data, so there is no time wasted
    int waitCounter = 0; // should be the number of workers

    // cycle through the directory
    for (directory_iterator itr(rootFolder); itr != end_itr; ++itr)
    {
        path currentFilePath = itr->path();

        // If it's not a subdirectory (and the file extension is .txt)
        if (is_regular_file(currentFilePath)
            && extension(currentFilePath) == ".txt")
        {
            // assign current file name to current_file
            string currentFile = currentFilePath.string();

            // LOG: INFO
            cout << "LEADER:: Pre-Processing File: " << currentFile << endl;

            string preProcessTempFilePath = "tmp/leader_preprocess.txt";

            // give all the workers their jobs first
            //    assumes 90 workers
            if (waitCounter >= 90)
            {
                if (!preProcessTextFileInMemory(currentFile, searchStringSize))
                {
                     // LOG: ERROR
                     cout << "LEADER:: Error: Failed to preprocess text file" << endl;
                }
            }
            else
            {
                 // if not over waitCounter, then just use actual
                 //    non preprocessed file path
                 preProcessTempFilePath = currentFilePath.string();
            }

            // increase (one more job sent)
            waitCounter++;


            // LOG: INFO
            cout << "LEADER:: Ready to process file: " << currentFile << endl;

    	    // Listen
            int BACKLOG = 1000; // don't allow connection queuing
    	    listen(socket_desc, BACKLOG);

            // LOG:INFO
    	    puts("LEADER:: Waiting for incoming connections from worker nodes...");
    	    c = sizeof(struct sockaddr_in);

    	    // Accept incoming connection from Worker Node
  	    if ((new_socket = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)))
    	    {
                // LOG: INFO
           	puts("LEADER:: Connection accepted.");
    	    }

            fflush(stdout);

    	    if (new_socket < 0)
    	    {
                // LOG: ERROR
         	perror("LEADER:: Error: Accept failed.");
         	return 1;
    	    }

            // METRICS: single file roundtrip time: START
      	    high_resolution_clock::time_point fileSendTimeStart = high_resolution_clock::now();

	    // !!!SEND THE JOB!!!
            send_text_file(new_socket, preProcessTempFilePath);

	    // METRICS: single file roundtrip time: STOP
            high_resolution_clock::time_point fileSendTimeStop = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>( fileSendTimeStop - fileSendTimeStart ).count();
            cout << "LEADER:: METRIC: Elapsed Time (File xfer): " << duration << " microseconds \n";

            // attempt to keep next bind from failing
            //std::this_thread::sleep_for (std::chrono::seconds(5));
        }

        // If it is a subdirectory
        if (!is_regular_file(itr->path()))
        {
            string current_folder = itr->path().string();
            cout << "LEADER:: Folder found: " << current_folder << endl;

            // call this method recursively
            iterateRootFolder(itr->path(), socket_desc);
        }

        // for any file that is not a subfolder or a .txt file, ignore it
    }
}




int main(int argc , char *argv[])
{

    // METRICS: totalRuntime time: START
    high_resolution_clock::time_point totalRuntimeStart = high_resolution_clock::now();

    //
    // Step 1: Bind the port to the socket
    //

    int socket_desc;
    struct sockaddr_in server;
    char *readin;

    //Create socket --- Naggle Algorithm (small data is collected for about 40ms and then sent)
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_desc == -1)
    {
       	puts("LEADER:: Error: Could not create socket.");
    }

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET; // address family
    server.sin_addr.s_addr = INADDR_ANY; // holds the ip address returned by inet_addr() to be used in the socket connection
    server.sin_port = htons( 8000 ); // the port

    // Bind to Leader
    if( bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
       	puts("LEADER:: Socket bind to port failed.");

      	return 1;
    }

    puts("LEADER:: Socket has been bound to port.");

    //
    // Get the list of file paths
    //

    // the boost filesystem way
    // list all files in current directory.
    //You could put any file path in here, e.g. "/home/me/mwah" to list that directory
    //path rootFolder ("/media/usb/round1/");

    string rootPathString = getMd5ChallengeRootPath();

    path rootFolder (rootPathString);

    iterateRootFolder(rootFolder, socket_desc);

    // Cleanup
    close(8000);
    fflush(stdout);

    cout << "Socket closed... stdout flushed.\n";

    // METRICS: totalRuntime: STOP
    high_resolution_clock::time_point totalRuntimeStop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>( totalRuntimeStop - totalRuntimeStart ).count();
    cout << "METRIC: Total Elapsed Time: " << duration << " microseconds \n";

    return 0;
}
