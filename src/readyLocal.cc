#include <boost/algorithm/string.hpp>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <errno.h>
#include <thread>
#include "../lib/md5.h"
#include <string>
#include <ctype.h>
#include <chrono>
#include <boost/chrono/time_point.hpp>
#include <boost/chrono/io/time_point_io.hpp>
#include <boost/chrono/chrono.hpp>
#include <wiringPi.h>

using namespace std::chrono;
using namespace std;
using std::string;



// On the Raspberry Pi models with a 40-pin GPIO headers,
// the Pi Traffic light connects to GPIO pins 9,10 and 11.
/*
pin 9 = Red LED
pin 10 = Yellow LED
pin 11 = Green LED
*/
void redLight(bool turnOn)
{
    wiringPiSetupGpio() ;
    pinMode (9, OUTPUT) ;

    if(turnOn)
    {
        // turn on
        digitalWrite (9, HIGH);
    }
    else
    {
        // turn off
        digitalWrite (9,  LOW);
    }
}
void yellowLight(bool turnOn)
{
    wiringPiSetupGpio() ;
    pinMode (10, OUTPUT) ;

    if(turnOn)
    {
        // turn on
        digitalWrite (10, HIGH);
    }
    else
    {
        // turn off
        digitalWrite (10,  LOW);
    }
}
void greenLight(bool turnOn)
{
    wiringPiSetupGpio() ;
    pinMode (11, OUTPUT) ;

    if(turnOn)
    {
        // turn on
        digitalWrite (11, HIGH);
    }
    else
    {
        // turn off
        digitalWrite (11,  LOW);
    }

}



int main(int argc , char *argv[])
{
    redLight(false);
    yellowLight(false);
    greenLight(true);
}
