#include <boost/algorithm/string.hpp>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <errno.h>
#include <thread>
#include "../lib/md5.h"
#include <string>
#include <ctype.h>
#include <chrono>
#include <boost/chrono/time_point.hpp>
#include <boost/chrono/io/time_point_io.hpp>
#include <boost/chrono/chrono.hpp>
#include <wiringPi.h>
#include "../lib/INIReader.h"

using namespace std::chrono;
using namespace std;
using std::string;










// INIReader.h --- https://github.com/jtilly/inih

//
// interact with the .ini file
//
int getMd5ChallengeSearchStringSize()
{
    // get the value from the .ini

    INIReader reader("config/cluster_config.ini");

    // make sure we can read something
    if (reader.ParseError() < 0)
    {
        std::cout << "WORKER:: Error: Can't load 'cluster_config.ini'.\n";

        return -1;
    }

    int searchStringSize = reader.GetInteger("Md5Challenge", "SearchStringSize", -1);

    std::cout << "WORKER:: Config loaded: [Md5Challenge] SearchStringSize="
              << searchStringSize << "\n";

    return searchStringSize;
}

//
// interact with the .ini file
//
string getMd5ChallengeTheHashToFind()
{
    // read the .ini into RAM
    INIReader reader("config/cluster_config.ini");

    // make sure we can read something
    if (reader.ParseError() < 0)
    {
        std::cout << "WORKER:: Error: Can't load 'cluster_config.ini'.\n";

        return "INI_ERROR";
    }

    string theHashToFind = reader.Get("Md5Challenge", "TheHashToFind", "INI_ERROR");

    std::cout << "WORKER:: Config loaded: [Md5Challenge] TheHashToFind="
              << theHashToFind << "\n";

    return theHashToFind;
}







// On the Raspberry Pi models with a 40-pin GPIO headers,
// the Pi Traffic light connects to GPIO pins 9,10 and 11.
/*
pin 9 = Red LED
pin 10 = Yellow LED
pin 11 = Green LED
*/
void redLight(bool turnOn)
{
    wiringPiSetupGpio() ;
    pinMode (9, OUTPUT) ;

    if(turnOn)
    {
        // turn on
        digitalWrite (9, HIGH);
    }
    else
    {
        // turn off
        digitalWrite (9,  LOW);
    }
}
void yellowLight(bool turnOn)
{
    wiringPiSetupGpio() ;
    pinMode (10, OUTPUT) ;

    if(turnOn)
    {
        // turn on
        digitalWrite (10, HIGH);
    }
    else
    {
        // turn off
        digitalWrite (10,  LOW);
    }
}
void greenLight(bool turnOn)
{
    wiringPiSetupGpio() ;
    pinMode (11, OUTPUT) ;

    if(turnOn)
    {
        // turn on
        digitalWrite (11, HIGH);
    }
    else
    {
        // turn off
        digitalWrite (11,  LOW);
    }

}



int writeWorkerStart(int workerNodeId)
{
    // create file name
    std::string const workerNodeFilePath = "log/worker" + std::to_string(workerNodeId) + "-Start.txt";
    char workerNodeFilePathChars[100];
    strcpy(workerNodeFilePathChars, workerNodeFilePath.c_str());

    // create time point
    boost::chrono::system_clock::time_point timePoint = boost::chrono::system_clock::now();

    // write to the file
    ofstream myfile;
    myfile.open (workerNodeFilePathChars);
    myfile << "worker started: \r\n";
    myfile << boost::chrono::time_fmt(boost::chrono::timezone::local);
    myfile << timePoint;
    myfile << "\r\n";
    myfile.close();

    return 0;
}




int writeWorkerComplete(int workerNodeId)
{
    // create file name
    std::string const workerNodeFilePath = "log/worker" + std::to_string(workerNodeId) + "-Complete.txt";
    char workerNodeFilePathChars[100];
    strcpy(workerNodeFilePathChars, workerNodeFilePath.c_str());

    // create time point
    boost::chrono::system_clock::time_point timePoint = boost::chrono::system_clock::now();

    // write to the file
    ofstream myfile;
    myfile.open (workerNodeFilePathChars);
    myfile << "worker complete: \r\n";
    myfile << boost::chrono::time_fmt(boost::chrono::timezone::local);
    myfile << timePoint;
    myfile << "\r\n";
    myfile.close();

    return 0;
}




int writeFound(string foundString, int workerNodeId)
{
    // turn on green light
    redLight(true);
    yellowLight(false);
    greenLight(false);

    // create file name
    std::string const workerNodeFilePath = "log/worker" + std::to_string(workerNodeId) + "-Found.txt";
    char workerNodeFilePathChars[100];
    strcpy(workerNodeFilePathChars, workerNodeFilePath.c_str());

    // write to the file
    ofstream myfile;
    myfile.open (workerNodeFilePathChars);
    myfile << "found the hash: \r\n";
    myfile << foundString;
    myfile << "\r\n";
    myfile.close();

    // write worker complete file
    writeWorkerComplete(workerNodeId);

    return 0;
}




int getWorkerNodeId()
{
    FILE *fp;
    unsigned char c1;

    if ((fp = fopen("/dev/urandom", "r")) == NULL) {
        fprintf(stderr, "WORKER:: Error: Could not open /dev/urandom for read.\n");
        return -1;
    }

    c1 = fgetc(fp);
    fclose(fp);

    printf("WORKER:: Random seed for WorkerNodeId: %d.\n", c1);

    // IMPORTANT!!! workers must spawn one after the other, with a small amount
    // of time in between or they'll try to use the same workerNodeId
    // which means things will break or act __really weird__
    // Symptom: data/ folder is not showing correct number of dataCache.txt
    // files (should be number of intended worker nodes)
    std::this_thread::sleep_for (std::chrono::milliseconds(100));

    srand(c1);

    int workerNodeId = rand() % 1000000;

    cout << "WORKER:: WorkerNodeId: " << workerNodeId << ".\n";

    return workerNodeId;
}




// read the file into memory, and then walk it, character by character
bool walkTextFileInMemory(int workerNodeId)
{
    /*
        Test Hashes:

        Beginning of the dataset:
                Hash: 394ed15fe3e4c72daf0bf955faf443a2
                String: Do so, but buy it f
                etext00/00ws110.txt

        End of the dataset:
                Hash: 17541b1cb2c7ec379c5d1a319cc4b425
                String: Whole BELLES ASSEMB
                etext98/wwrld10.txt
    */
    //string theHashToFind = "c1652d8a09bb6be29f6a7aa35c733bb8";
    //string theHashToFind = "59bb5beee293c5ee42638b8d73dc6676";
    //string theHashToFind = "69e5ac0cf03478d7e20b6fd3c7451c6f";
    //string theHashToFind = getMd5ChallengeTheHashToFind();
    //int searchStringSize = getMd5ChallengeSearchStringSize();
    //string theHashToFind = "ed46224a9526d0f7f993cadbbc253059"; // dan 2.5 minutes
    //int searchStringSize = 25;
    string theHashToFind = "391e43cb0d985c7ce0c76fd6ea4e822b";
    int searchStringSize = 21;

    puts("WORKER:: Start method walkTextFileInMemory()..."); // add workernode Id to all of these logs

    // DEBUG
    //cout << theHashToFind << " " << searchStringSize;

    MD5 md5;

    char currentSearchWindow[searchStringSize];
    char newSearchWindow[searchStringSize];
    char currentCharacter;
    int searchWindowIndex = 0;

    // create file path for disk cache
    std::string const workerNodeFilePath = "data/diskCache-" + std::to_string(workerNodeId) + ".txt";
    char workerNodeFilePathChars[100];
    strcpy(workerNodeFilePathChars, workerNodeFilePath.c_str());

    // read whole file into memory (string)
    std::ifstream textFileIn(workerNodeFilePathChars);
    std::string textFileContents((std::istreambuf_iterator<char>(textFileIn)),
                                  std::istreambuf_iterator<char>());

    // LOG: DEBUG
    //cout << "WHAT THE FUCK" << endl;

    // convert from string to char[]
    int textFileLengthInChars = textFileContents.length();
    char inMemoryCache[textFileLengthInChars + 1];
    strcpy(inMemoryCache, textFileContents.c_str());

    // walk each character of the in memory cache
    for(int textFileIndex = 0; textFileIndex < textFileLengthInChars; textFileIndex++)
    {
        if (searchWindowIndex < searchStringSize)
        {
            // DEBUG
            //puts("Building initial searchWindow...");

            // populate our first search window fully before checking hashes
            newSearchWindow[searchWindowIndex] = inMemoryCache[textFileIndex];

            searchWindowIndex++;

            continue;
        }

        // copy the updated array to the working array
        strncpy(currentSearchWindow, newSearchWindow, sizeof(currentSearchWindow));

        // DEBUG
        //cout << "Current Search Window: " << currentSearchWindow << "\n";

        // DEBUG: sleep so i can see what's happening
        //std::this_thread::sleep_for (std::chrono::seconds(1));

        // if we got here, we have a searchable window, so do it
        if (md5.digestString(currentSearchWindow) == theHashToFind)
        {
            puts("\n\n\n\n\n\n\nWORKER:: FOUND IT\n\n\n\n\n\n\n");

            writeFound(currentSearchWindow, workerNodeId);

            return true;
        }
        else
        {
           // DEBUG
           //puts("Rolling searchWindow forward one character...");

           //create newSearchWindow, moving one character forward in the text
           // copy (searchStringSize-1) elements * 8 bytes (char)
           //    starting at element 1 of source, to element 0 of destination
           memcpy(&newSearchWindow[0], &currentSearchWindow[1], (searchStringSize-1) * 8);

           // DEBUG
           //cout << "Next character is: " << textFileIndex << "\n";

           // then add the next character to the end of the array
           newSearchWindow[(searchStringSize-1)] = inMemoryCache[textFileIndex]; // add new character to end of array
        }
    }

    puts("WORKER:: Completed collision calculation for this Text File.");

    puts("WORKER:: End method walkTextFileInMemory().");

    // we didn't find the hash in this file
    return false;
}


// walk the file on disk, character by character
bool walkTextFileOnDisk(int workerNodeId)
{
    /*
        Test Hashes:

        Beginning of the dataset:
                Your Hash: 17541b1cb2c7ec379c5d1a319cc4b425
                Your String: Whole BELLES ASSEMB
                etext98/wwrld10.txt

        End of the dataset:
                Your Hash: 394ed15fe3e4c72daf0bf955faf443a2
                Your String: Do so, but buy it f
                etext00/00ws110.txt
    */
    //string theHashToFind = "c1652d8a09bb6be29f6a7aa35c733bb8";
    //string theHashToFind = "59bb5beee293c5ee42638b8d73dc6676";
    //string theHashToFind = "69e5ac0cf03478d7e20b6fd3c7451c6f";
    string theHashToFind = getMd5ChallengeTheHashToFind();
    int searchStringSize = getMd5ChallengeSearchStringSize();

    puts("WORKER:: Start method walkTextFileOnDisk()..."); // add workernode Id to all of these logs

    // DEBUG
    cout << theHashToFind << " " << searchStringSize;

    MD5 md5;

    char currentSearchWindow[searchStringSize];
    char newSearchWindow[searchStringSize];
    char currentCharacter;
    int searchWindowIndex = 0;

    std::string const workerNodeFilePath = "data/diskCache-" + std::to_string(workerNodeId) + ".txt";
    char workerNodeFilePathChars[100];
    strcpy(workerNodeFilePathChars, workerNodeFilePath.c_str());

    // DEBUG
    //cout << workerNodeFilePathChars << "\n";

    fstream fin(workerNodeFilePathChars, fstream::in);

    // for each character in the file
    while (fin >> noskipws >> currentCharacter)
    {
        if (searchWindowIndex < searchStringSize)
        {
            // DEBUG
            //puts("Building initial searchWindow...");

            // populate our first search window fully before checking hashes
            newSearchWindow[searchWindowIndex] = currentCharacter;
            searchWindowIndex++;
            continue;
        }

        // copy the updated array to the working array
        strncpy(currentSearchWindow, newSearchWindow, sizeof(currentSearchWindow));

        // =============================================
        // TESTING memcpy FOR PERFORMANCE -- di0
        //memcpy(currentSearchWindow, newSearchWindow, 19);
        //memcpy(&currentSearchWindow[0], &newSearchWindow[0], 19 * 8);
        // ====> WAS 15 SECONDS SLOWER --- REVERTING

        // DEBUG
        ///cout << "Current Search Window: " << currentSearchWindow << "\n";

        // DEBUG: sleep so i can see what's happening
        //std::this_thread::sleep_for (std::chrono::seconds(1));

        // if we got here, we have a searchable window, so do it
        if (md5.digestString(currentSearchWindow) == theHashToFind)
        {
            puts("\n\n\n\n\n\n\nWORKER:: FOUND IT\n\n\n\n\n\n\n");

            writeFound(currentSearchWindow, workerNodeId);

            return true;
        }
        else
        {
           // DEBUG
           //puts("Rolling searchWindow forward one character...");

           //create newSearchWindow, moving one character forward in the text

           // apparently strncpy is slow, so don't do it
           //strncpy(newSearchWindow + 1, currentSearchWindow, (searchStringSize-1));

           // copy (searchStringSize-1) elements * 8 bytes (char)
           //    starting at element 1 of source, to element 0 of destination
           memcpy(&newSearchWindow[0], &currentSearchWindow[1], (searchStringSize-1) * 8);
           // PERFORMANCE TESTING strncpy
           //strncpy(&newSearchWindow[0], &currentSearchWindow[1], (searchStringSize-1));

           // DEBUG
           //cout << "Next character is: " << currentCharacter << "\n";

           // then add the next character to the end of the array
           newSearchWindow[(searchStringSize-1)] = currentCharacter; // add new character to end of array
        }
    }

    puts("WORKER:: Completed collision calculation for this Text File.");

    puts("WORKER:: End method walkTextFile().");

    // we didn't find the hash in this file
    return false;
}




//This function is to be used once we have confirmed that an Text File is to be sent
//It should read and output a Text File
int socketReceiveTextFile(int socket_desc, int workerNodeId)
{
    puts("WORKER:: Start method socketReceiveTextFile()...");

    int buffersize = 0,
        recv_size = 0,
        size = 0,
        read_size,
        write_size,
        packet_index = 1,
        stat;
    char textFileArray[10240],
         verify = '1';
    FILE *textFile;

    puts("WORKER:: Finding the size of the Text File...");

    //Find the size of the textFile
    do
    {
        stat = read(socket_desc, &size, sizeof(int));
    }
    while (stat < 0);

    printf("WORKER:: ext File size (bytes) = %d.\n", size) ;

    // this initialization text doesn't seem necessary -- di0
    char buffer[] = "Got it";

    //Send our verification signal
    puts("WORKER:: Sending verification signal...");

    // DEBUG
    int statCount = 0;
    do
    {
        stat = write(socket_desc, &buffer, sizeof(int));

        // DEBUG
        cout << "statCount= " << statCount << "\n";
        statCount++;
    }
    while (stat < 0);

    puts("WORKER:: Verification signal sent to leader.");

    char fn[100];

    std::string const workerNodeFilePath = "data/diskCache-" + std::to_string(workerNodeId) + ".txt";
    char workerNodeFilePathChars[100];

    strcpy(workerNodeFilePathChars, workerNodeFilePath.c_str());
    // TESTING PERFORMANCE OF memcopy vs strncpy ---- di0
    //memcpy(&workerNodeFilePathChars[0], workerNodeFilePath.c_str(), sizeof(workerNodeFilePathChars));
    // ====> WAS 15 SECONDS SLOWER --- REVERTING

    // DEBUG
    cout << "WORKER:: Worker node disk cache: " << workerNodeFilePathChars << "\n";

    // TODO: why am i doing this dumb array copy? ---- di0
    //sprintf(fn, "%s", "data/diskCache-123123.txt");
    sprintf(fn, "%s", workerNodeFilePathChars);

    puts("WORKER:: Opening Text File...");

    textFile = fopen(fn, "wb");

    if(textFile == NULL)
    {
        puts("WORKER:: Error has occurred. Text File file could not be opened.");
        return -1;
    }

    puts("WORKER:: Text File opened successfully.");

    int need_exit = 0;
    struct timeval timeout = {10,0};
    fd_set fds;
    int buffer_fd, buffer_out;

    //Loop while we have not received the entire file yet
    while(recv_size < size)
    {
        //while(packet_index < 2){
        FD_ZERO(&fds);
        FD_SET(socket_desc, &fds);

        buffer_fd = select(FD_SETSIZE, &fds, NULL, NULL, &timeout);

        if (buffer_fd < 0)
             puts("WORKER:: Error: Bad file descriptor set.");

        if (buffer_fd == 0)
             puts("WORKER:: Error: Buffer read timeout expired.");

        if (buffer_fd > 0)
        {
             do
             {
                 read_size = read(socket_desc, textFileArray, 10240);
             }
             while(read_size <0);

             // DEBUG - don't delete - di0
             //printf("Packet number received: %i\n",packet_index);
             //printf("Packet size: %i\n",read_size);

             //Write the currently read data into our Text File
             write_size = fwrite(textFileArray,1,read_size, textFile);

             // DEBUG
             //printf("Written Text File size: %i\n",write_size);

             if(read_size !=write_size)
             {
                 puts("WORKER:: Error: read_size does not match write_size.");
             }

             //Increment the total number of bytes read
             recv_size += read_size;
             packet_index++;

             // DEBUG
             //printf("Total received Text File size: %i\n",recv_size);
             //printf(" \n");
             //printf(" \n");
        }
    }

    fclose(textFile);

    puts("WORKER:: Text File successfully Received! Connection closed.");

    //
    // Walk the text file we just received from the server
    //

    if (walkTextFileInMemory(workerNodeId))
    {
        puts("WORKER:: Reported success in completing the challenge");

        return 5555; // custom return code for success - TODO: declare up top - di0
    }

    puts("WORKER:: End method socketReceiveTextFile().");

    return 0;
}




int main(int argc , char *argv[])
{
    int socket_desc;
    struct sockaddr_in server;
    char *parray;

    // set random worker node ID
    int workerNodeId = getWorkerNodeId();

    // record worker start datetime
    writeWorkerStart(workerNodeId);

    // turn on yellow light (processing)
    redLight(false);
    yellowLight(true);
    greenLight(false);

    while (true)
    {
        printf("WORKER:: Creating socket...\n");

        //Create socket
        socket_desc = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_desc == -1)
        {
            printf("WORKER:: Error: Could not create socket\n");
        }

        printf("WORKER:: Socket created.\n");

        memset(&server, 0, sizeof(server));
        server.sin_addr.s_addr = inet_addr("192.168.111.101");
        server.sin_family = AF_INET;
        server.sin_port = htons( 8000 );

        printf("WORKER:: Connecting to leader...\n");

        //Connect to remote server
        if (connect(socket_desc, (struct sockaddr *)&server , sizeof(server)) < 0)
        {
            cout << strerror(errno);

            close(socket_desc);

            puts("WORKER:: Error: Cannot connect to leader.\n");

            return 1;
        }

        puts("WORKER:: Connected to leader.");

        puts("WORKER:: Receiving Text File...");

        // TRY TO SPEED THINGS UP
        //int val = true;
        //if (setsockopt(infd, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val)) == -1) { ... }

        //puts("socket_desc == ");
        //cout << socket_desc;
        if (socketReceiveTextFile(socket_desc, workerNodeId) == 5555)
        {
            puts("WORKER:: FOUND IT\n");

            puts("WORKER:: Closing socket.\n\n\n");

            close((int)socket_desc);

            return 0;
        }

        puts("WORKER:: Closing socket...\n\n\n");

        close((int)socket_desc);
    }

    puts("WORKER:: Socket closed. Exiting program.\n");

    return 0;
}
