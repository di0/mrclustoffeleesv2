#!/bin/bash

# =====> clear out run data =====>
echo "DEPLOY:: clearing out old run data"
# might want to move this logic to 'cleanLocal.sh' and 'cleanAll.sh'
rm data/*
rm log/*

