#!/bin/bash

# ====> reboot if --noreboot param does not exist =====>
if [ $# -eq 1 ] && [ $1 == "--noreboot" ]; then
    # do not reboot
    ssh pi@192.168.111.102 /home/pi/tempDev/mrclustoffeleesv2/util/deployLocal.sh --noreboot
    ssh pi@192.168.111.103 /home/pi/tempDev/mrclustoffeleesv2/util/deployLocal.sh --noreboot
    /home/pi/tempDev/mrclustoffeleesv2/util/deployLocal.sh --noreboot
else
    # do reboot
    ssh pi@192.168.111.102 /home/pi/tempDev/mrclustoffeleesv2/util/deployLocal.sh
    ssh pi@192.168.111.103 /home/pi/tempDev/mrclustoffeleesv2/util/deployLocal.sh
    /home/pi/tempDev/mrclustoffeleesv2/util/deployLocal.sh
fi



