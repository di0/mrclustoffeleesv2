#!/bin/bash


#
#
# The purpose of this script is to update the source for this repo 
# and build the respective binaries (bin/worker, bin/leader)
#
#



# get to the right working directory (affects screen default folder)
echo "DEPLOY:: changing to correct working directory"
cd ~/tempDev/mrclustoffeleesv2



# ====> clean up old run files =====>
echo "DEPLOY:: clean up old run files"
util/cleanLocal.sh



# ====> update to latest source code =====>
echo "DEPLOY:: update to latest source code"
# TODO: change these to ssh remote commands (so deployAll.sh will update the repo before running this script
git stash
git pull



# ====> rebuild worker =====>
echo "DEPLOY:: rebuild worker"
# -w disables warnings
g++ -Wno-psabi -g -w src/worker.cc -o bin/worker -L/usr/local/lib/ -lboost_filesystem -lboost_system -lboost_chrono -lwiringPi

# ====> rebuild leader =====>
echo "DEPLOY:: rebuild leader"
# -w disables warnings
# -Wno-psabi disables GCC change warnings
g++ -Wno-psabi -g -w src/leader.cc -o bin/leader -L/usr/local/lib/ -lboost_filesystem -lboost_system -lboost_chrono -lwiringPi

# ====> rebuild readyLocal =====>
echo "DEPLOY:: rebuild readyLocal"
# -w disables warnings
# -Wno-psabi disables GCC change warnings
g++ -Wno-psabi -g -w src/readyLocal.cc -o bin/readyLocal -L/usr/local/lib/ -lboost_filesystem -lboost_system -lboost_chrono -lwiringPi




# ====> reboot if --noreboot param does not exist =====>
if [ $# -eq 1 ] && [ $1 == "--noreboot" ]; then
    # do not reboot
    echo "DEPLOY:: COMPLETE."
else
    # do reboot
    echo "DEPLOY:: COMPLETE. Restarting..."
    sudo reboot
fi
