#!/bin/bash


#
#
# The purpose of this script is to spawn the cluster binaries ('bin/worker', 'bin/leader')
# in their own respective screen windows.
#
# NOTE: 'util/deployLocal.sh' should be run first to make sure all code is up to date.
#
#

# ====> error if 2 params do not exist =====>
if [ $# -ne 2 ]; then
    # prompt user to provide correct params
    echo "DEPLOY:: Error: Please pass params, util/runlocal.sh <#leaders> <#workers>"
    exit 1
fi

# ====> change to correct folder =====>
echo "RUN:: changing to correct working directory"
cd ~/tempDev/mrclustoffeleesv2

# ====> clean up old run files =====>
echo "RUN:: clean up old run files"
util/cleanLocal.sh

# ====> configure the cluster =====>
echo "RUN:: configure the cluster"

leaderCount=$1 # args[0]
workerCount=$2 # args[1]
runLeader="bin/leader;"
runWorker="bin/worker;"
runScreen="screen"
clusterInitString=""

# add the appropriate number of leaders to the init string
echo "RUN:: initializing $1 leaders"
for ((i=1;i<=$leaderCount;i++)); do
    echo $i
    clusterInitString="$clusterInitString $runScreen $runLeader"
done

echo "RUN:: initializing $2 workers"
# add the appropriate number of workers to the init string
for ((i=1;i<=$workerCount;i++)); do
    echo $i
    clusterInitString="$clusterInitString $runScreen $runWorker"
done

echo "RUN:: initializing cluster with init string:"
echo "${clusterInitString[0]}"

# start the cluster in a bunch of separate screen windows (same session though)
# DEBUG: the actual command looks like this:
# screen -dmS screensession bash -c "screen ./leader; screen ./worker; screen ./worker; screen ./worke$
screen -dmS screensession bash -c "${clusterInitString[0]}"

echo "RUN:: COMPLETE."
